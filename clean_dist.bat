SET mypath=%~dp0
cd %mypath:~0,-1%
del dist\*.map
del dist\*.gz
del dist\server\*.map
del dist\server\*.gz
del dist\browser\*.gz
#del dist\browser\*.js
#del dist\browser\*.css
del dist\browser\assets\css\*.css
del dist\browser\assets\js\*.js

cd dist
cd browser
del index.html
rename index.min.html index.html
del policy.html
rename policy.min.html policy.html
del service.html
rename service.min.html service.html