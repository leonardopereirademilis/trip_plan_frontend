"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("zone.js/dist/zone-node");
var core_1 = require("@angular/core");
var platform_server_1 = require("@angular/platform-server");
var path_1 = require("path");
var fs_1 = require("fs");
var tokens_1 = require("@nguniversal/express-engine/tokens");
var module_map_ngfactory_loader_1 = require("@nguniversal/module-map-ngfactory-loader");
var compression = require('compression');
var http = require('http');
// const https = require('https');
var DIST_FOLDER = '';
if (process.cwd().includes('trip_plan_frontend')) {
    DIST_FOLDER = path_1.join(process.cwd(), 'dist');
}
else {
    DIST_FOLDER = path_1.join(process.cwd(), 'trip_plan_frontend', 'dist');
}
// const privateKey = readFileSync(join(DIST_FOLDER, 'browser', 'certificate', 'server.key'), 'utf8');
// const certificate = readFileSync(join(DIST_FOLDER, 'browser', 'certificate', 'server.cert'), 'utf8');
// const credentials = {key: privateKey, cert: certificate};
var express = require('express');
// Faster server renders w/ Prod mode (dev mode never needed)
core_1.enableProdMode();
// Express server
var app = express();
app.use(compression());
var PORT_HTTP = process.env.NODE_FRONTEND_PORT_HTTP || 8080;
// const PORT_HTTPS = process.env.NODE_FRONTEND_PORT_HTTPS || 443;
// * NOTE :: leave this as require() since this file is built Dynamically from webpack
var _a = require('./server/main'), AppServerModuleNgFactory = _a.AppServerModuleNgFactory, LAZY_MODULE_MAP = _a.LAZY_MODULE_MAP;
var template = fs_1.readFileSync('./dist/browser/index.html').toString();
app.engine('html', function (_, options, callback) {
    platform_server_1.renderModuleFactory(AppServerModuleNgFactory, {
        // Our index.html
        document: template,
        url: options.req.url,
        extraProviders: [
            module_map_ngfactory_loader_1.provideModuleMap(LAZY_MODULE_MAP),
            // make req and response accessible when angular app runs on server
            {
                provide: tokens_1.REQUEST,
                useValue: options.req
            },
            {
                provide: tokens_1.RESPONSE,
                useValue: options.req.res,
            },
        ]
    }).then(function (html) {
        callback(null, html);
    });
});
app.set('view engine', 'html');
app.set('views', path_1.join(DIST_FOLDER, 'browser'));
function wwwRedirect(req, res, next) {
    if (req.headers.host.slice(0, 4) !== 'www.') {
        return res.redirect(301, 'http://www.' + req.headers.host + req.url);
    }
    next();
}
function indexRedirect(req, res, next) {
    if (req.url.indexOf('/index.html') !== -1) {
        var url = 'http://' + req.headers.host + req.url.replace('/index.html', '');
        return res.redirect(301, url);
    }
    next();
}
app.set('trust proxy', true);
app.use(wwwRedirect);
app.use(indexRedirect);
// Server static files from /browser
app.get('*.*', express.static(path_1.join(DIST_FOLDER, 'browser'), {
    maxAge: '1y'
}));
// All regular routes use the Universal engine
app.get('*', function (req, res) {
    res.render('index', { req: req });
});
var httpServer = http.createServer(app);
// const httpsServer = https.createServer(credentials, app);
// Start up the Node server
httpServer.listen(PORT_HTTP, function () {
    console.log("Node Express server listening on http://localhost:" + PORT_HTTP);
});
// httpsServer.listen(PORT_HTTPS, () => {
//      console.log(`Node Express server listening on https://localhost:${PORT_HTTPS}`);
// });
//# sourceMappingURL=server.js.map