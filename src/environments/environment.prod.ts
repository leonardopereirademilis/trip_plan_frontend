export const environment = {
    production: true,
    envName: 'prod',
    APP_CONFIG_IMPL: {
        apiUrl: 'http://gastosdeviagemloadbalancerBE-61154410.sa-east-1.elb.amazonaws.com'
    }
};
