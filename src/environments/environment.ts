export const environment = {
    production: false,
    envName: 'dev',
    APP_CONFIG_IMPL: {
        apiUrl: 'http://localhost:3000'
    }
};
