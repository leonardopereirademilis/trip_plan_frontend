﻿import {isPlatformBrowser} from '@angular/common';
import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthenticationService} from '../users/authentication.service';


@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {
    constructor(private router: Router,
                private authenticationService: AuthenticationService,
                @Inject(PLATFORM_ID) private platformId: any) {
    }

    canActivate(activatedRouteSnapshot: ActivatedRouteSnapshot, routerStateSnapshot: RouterStateSnapshot) {
        if (isPlatformBrowser(this.platformId)) {
            const currentUser = this.authenticationService.currentUserValue;
            if (currentUser) {
                // authorised so return true
                return true;
            }
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login'], {queryParams: {returnUrl: routerStateSnapshot.url}});
        return false;
    }
}
