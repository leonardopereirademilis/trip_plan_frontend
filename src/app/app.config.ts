import {InjectionToken} from '@angular/core';

export class AppConfig {
  apiUrl: string;
}

export let APP_CONFIG = new InjectionToken<AppConfig>('app.config');
