import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Item} from '../trips/trip/items/item';
import {Trip} from '../trips/trip/trip';

@Injectable({
    providedIn: 'root'
})
export class DemoService {
    demoTrips: Trip[] = [];
    currentDemoTrip: Trip;
    demoItems: Item[] = [];
    demoNote: string;

    constructor(private translate: TranslateService) {
    }

    loadDemoTrips(): Trip[] {
        this.demoTrips = [];

        const demoTrip1 = new Trip();
        demoTrip1.tripName = 'demoTripName1';
        demoTrip1.tripDateStart = new Date();
        demoTrip1.tripDateEnd = new Date();
        demoTrip1.tripDateStart.setDate(demoTrip1.tripDateStart.getDate() + 40);
        demoTrip1.tripDateEnd.setDate(demoTrip1.tripDateEnd.getDate() + 55);
        this.demoTrips.push(demoTrip1);

        const demoTrip2 = new Trip();
        demoTrip2.tripName = 'demoTripName2';
        this.demoTrips.push(demoTrip2);

        this.currentDemoTrip = demoTrip1;

        this.translateDemoTrips();
        this.loadAllDemoItems();

        return this.demoTrips;
    }

    translateDemoTrips() {
        this.demoTrips.forEach(demoTrip => {
            this.translate.get(demoTrip.tripName).subscribe(value => {
                demoTrip.tripName = value;
            });
        });
    }

    loadAllDemoItems(): Item[] {
        this.demoItems = [];
        this.addItems('budget', 3, 1200);
        this.addItems('flight', 2, 550);
        this.addItems('accommodation', 3, 400);
        this.addItems('car', 1, 350);
        this.addItems('ticket', 2, 150);
        this.addItems('insurance', 1, 100);
        this.addItems('exchange', 2, 700);
        this.addItems('other', 3, 100);

        this.translateDemoItems();

        return this.demoItems;
    }

    translateDemoItems() {
        this.demoItems.forEach(demoItem => {
            this.translate.get(demoItem.itemName).subscribe(value => {
                demoItem.itemName = value;
            });
        });
    }

    private addItems(itemType: string, quant: number, value: number) {
        for (let i = 1; i <= quant; i++) {
            const now = new Date();
            const itemDate = new Date(now.getFullYear(), now.getMonth(), 1, 0, 0, 0, 0);
            itemDate.setMonth(itemDate.getMonth() - 2 + i);
            const item = this.buidItem(itemType,
                itemType + i + 'ItemName',
                this.currentDemoTrip,
                value * i,
                (value * i) / (3.7 + (i / 10)),
                itemType === 'budget' ? 1 : i,
                itemDate,
                itemType === 'budget' && itemDate.getFullYear() <= now.getFullYear() && itemDate.getMonth() <= now.getMonth(),
                'Dolar');

            this.demoItems.push(item);
        }
    }

    buidItem(itemType: string,
             itemName: string,
             trip: Trip,
             value: number,
             exchangeValue: number,
             times: number,
             itemDate: Date,
             itemOk: boolean,
             selectedCurrency: string): Item {

        const item = new Item();
        item.itemType = itemType;
        item.itemName = itemName;
        item.trip = trip;
        item.value = value;
        item.exchangeValue = exchangeValue;
        item.times = times;
        item.itemDate = itemDate;
        item.itemOk = itemOk;
        item.selectedCurrency = selectedCurrency;

        return item;
    }

    loadDemoNote() {
        this.translate.get('demoNote').subscribe(value => {
            this.demoNote = value;
        });
    }

}
