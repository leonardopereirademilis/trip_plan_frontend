import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Item } from '../trips/trip/items/item';

@Component({
    selector: 'app-chartjs',
    templateUrl: './chartjs.component.html',
    styleUrls: ['./chartjs.component.css']
})
export class ChartjsComponent implements OnInit {
    @Input() items: Item[] = [];
    @Input() itemsValueTotal = [];
    @Input() itemsValuePayed = [];
    @Input() itemsValueCurrent = [];
    @Input() itemsValuePending = [];
    @Input() itemsIndex = [];

    payedAmount: string;
    currentAmount: string;
    pendingAmount: string;
    totalAmount: string;

    constructor(private translate: TranslateService) {
    }

    // doughnutChart
    public doughnutChartLabels = [];
    public doughnutChartData = [];
    public doughnutChartType = 'doughnut';
    public doughuntOptions = {
        tooltips: {
            callbacks: {
                label: function (tooltipItem, data) {
                    const dataset = data.datasets[tooltipItem.datasetIndex];
                    const meta = dataset._meta[Object.keys(dataset._meta)[0]];
                    const total = meta.total;
                    const currentValue = dataset.data[tooltipItem.index];
                    const percentage = parseFloat((currentValue / total * 100).toFixed(1));
                    return currentValue.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) + ' (' + percentage + '%)';
                },
                title: function (tooltipItem, data) {
                    return data.labels[tooltipItem[0].index];
                }
            }
        },
    };

    // doughnutChart

    // barChart
    public barChartOptions = {
        scaleShowVerticalLines: false,
        responsive: true,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    userCallback: function (value, index, values) {
                        return value.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
                    }
                }
            }]
        },
        tooltips: {
            callbacks: {
                label: function (tooltipItem, chart) {
                    const datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                    return datasetLabel + ': ' + tooltipItem.yLabel.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
                }
            }
        }
    };
    public barChartLabels = [];
    public barChartType = 'bar';
    public barChartLegend = true;
    public barChartData = [];
    // barChart

    ngOnInit() {
        this.translate.get('Payed amount').subscribe(value => {
            this.payedAmount = value;
        });
        this.translate.get('Current month amount').subscribe(value => {
            this.currentAmount = value;
        });
        this.translate.get('Pending amount').subscribe(value => {
            this.pendingAmount = value;
        });
        this.translate.get('Total').subscribe(value => {
            this.totalAmount = value;
        });

        this.translateIndex();

    }

    translateIndex() {
        let indexOk = 0;
        this.itemsIndex.forEach(index => {
            this.translate.get(this.capitalize(index)).subscribe((res: string) => {
                this.itemsIndex[indexOk] = res;
                indexOk++;

                if (indexOk === this.itemsIndex.length) {
                    this.generateChart();
                }
            });
        });
    }

    capitalize(myString: string): string {
        return myString.charAt(0).toUpperCase() + myString.slice(1);
    }

    generateChart() {
        this.doughnutChartLabels = this.itemsIndex;
        this.doughnutChartData = this.itemsValueTotal;
        this.barChartLabels = this.itemsIndex;
        this.barChartData = [
            { data: this.itemsValueTotal, label: this.totalAmount }
            , { data: this.itemsValuePayed, label: this.payedAmount }
            , { data: this.itemsValueCurrent, label: this.currentAmount }
            , { data: this.itemsValuePending, label: this.pendingAmount }
        ];
    }
}
