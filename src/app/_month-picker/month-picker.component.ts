import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbDropdown, NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-month-picker',
  templateUrl: './month-picker.component.html',
  styleUrls: ['./month-picker.component.css'],
  providers: [NgbDropdownConfig]
})
export class MonthPickerComponent implements OnInit {
  @Input() date: Date;
  @Input() disabled = false;
  @Input() invalid = false;
  @Input() editable = false;
  @Input() errorMessage = 'Date is required';
  @Output() selectMonthEmitter = new EventEmitter<Date>();

  selectedDate: Date;
  selectedMonth: number;
  selectedYear: number;
  selectedMonthString: string;
  currentDate = new Date();

  months: string[] = ['January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'];

  constructor(config: NgbDropdownConfig, private translate: TranslateService) {
    config.placement = 'bottom-right';
    config.autoClose = 'outside';
  }

  ngOnInit() {
    if (this.date) {
      this.selectedDate = new Date(this.date);
      this.selectedMonth = this.selectedDate.getMonth();
      this.selectedYear = this.selectedDate.getFullYear();
      this.updateInput();
    } else {
      this.selectedMonth = new Date().getMonth();
      this.selectedYear = new Date().getFullYear();
    }

  }

  selectMonth(myDrop: NgbDropdown) {
    this.selectedDate = new Date(this.selectedYear, this.selectedMonth, 1, 0, 0, 0, 0);
    this.updateInput();
    this.selectMonthEmitter.emit(this.selectedDate);
    myDrop.close();
  }

  private updateInput() {
    if (this.selectedMonth) {
      this.translate.get(this.months[this.selectedMonth]).subscribe((res: string) => {
        this.selectedMonthString = res + '/' + this.selectedYear;
      });
    }
  }

  nextMonth() {
    if (this.selectedMonth === 11) {
      this.selectedMonth = 0;
      this.selectedYear++;
    } else {
      this.selectedMonth++;
    }
  }

  previousMonth() {
    if (this.selectedMonth === 0) {
      this.selectedMonth = 11;
      this.selectedYear--;
    } else {
      this.selectedMonth--;
    }
  }
}
