import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Trip} from '../trip/trip';
import {NgbActiveModal, NgbDateParserFormatter, NgbDatepickerI18n, NgbDateStruct, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TripService} from '../trip.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators/first';
import {I18n, NgbDatepickerI18nBR} from '../../_helpers/ngb-datepicker-i18n-br';
import {NgbDateBRParserFormatter} from '../../_helpers/ngb-date-br-parser-formatter';
import {UserService} from '../../users/user.service';

@Component({
    selector: 'app-new-trip',
    templateUrl: './new-trip.component.html',
    styleUrls: ['./new-trip.component.css'],
    providers: [
        I18n,
        {provide: NgbDatepickerI18n, useClass: NgbDatepickerI18nBR},
        {provide: NgbDateParserFormatter, useClass: NgbDateBRParserFormatter}
    ]
})
export class NewTripComponent implements OnInit {
    registerForm: FormGroup;
    newTrip: Trip = new Trip();
    today: NgbDateStruct;
    tomorrow: NgbDateStruct;
    minDate: NgbDateStruct;
    submitted = false;
    updateDate = true;
    internalError: string = null;

    @Output() loadAllTrips = new EventEmitter();

    constructor(private formBuilder: FormBuilder,
                private modalService: NgbModal,
                private userService: UserService,
                private tripService: TripService) {
    }

    ngOnInit() {
        const now = new Date();
        this.today = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
        this.tomorrow = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() + 1};
        this.minDate = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.registerForm.controls;
    }

    open(content: any) {
        this.modalService.dismissAll();
        this.submitted = false;
        this.internalError = null;
        this.newTrip = new Trip();
        this.newTrip.user = this.userService.currentUser;

        this.registerForm = this.formBuilder.group({
            tripName: ['', Validators.required],
            tripDateStart: [],
            tripDateEnd: []
        });

        this.modalService.open(content, {size: 'lg', centered: true}).result.then((result) => {
            this.newTrip = new Trip();
            this.loadAllTrips.emit();
        }, (reason) => {
            this.newTrip = new Trip();
            this.registerForm.get('tripName').setValue(null);
            this.registerForm.get('tripDateStart').setValue(null);
            this.registerForm.get('tripDateEnd').setValue(null);
            this.submitted = false;
        });
    }

    updateTripDate(): void {
        if (this.updateDate) {
            if (this.registerForm.value.tripDateStart) {
                this.newTrip.tripDateStart = new Date(this.registerForm.value.tripDateStart.year, this.registerForm.value.tripDateStart.month - 1, this.registerForm.value.tripDateStart.day);
                this.minDate = this.registerForm.value.tripDateStart;
            }
            if (this.registerForm.value.tripDateEnd) {
                this.newTrip.tripDateEnd = new Date(this.registerForm.value.tripDateEnd.year, this.registerForm.value.tripDateEnd.month - 1, this.registerForm.value.tripDateEnd.day);
            }
            if (this.newTrip.tripDateStart > this.newTrip.tripDateEnd) {
                const dateEnd: NgbDateStruct = {
                    year: new Date(this.newTrip.tripDateStart).getFullYear(),
                    month: new Date(this.newTrip.tripDateStart).getMonth() + 1,
                    day: new Date(this.newTrip.tripDateStart).getDate() + 1
                };

                this.updateDate = false;
                this.registerForm.get('tripDateEnd').setValue(dateEnd);
                this.newTrip.tripDateEnd = new Date(dateEnd.year, dateEnd.month - 1, dateEnd.day);
            }
        } else {
            this.updateDate = true;
        }
    }

    onSubmit(modal: NgbActiveModal) {
        this.internalError = null;
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        this.newTrip.user = this.userService.currentUser;
        this.newTrip.tripName = this.registerForm.value.tripName;

        this.tripService.create(this.newTrip).pipe(first()).subscribe(() => {
            modal.close();
        }, err => {
            this.internalError = err;
        });
    }

}
