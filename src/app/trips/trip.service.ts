import {HttpClient} from '@angular/common/http';
import {Inject, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators/first';
import {AppConfig, APP_CONFIG} from '../app.config';
import {Trip} from './trip/trip';

@Injectable({
    providedIn: 'root'
})
export class TripService {
    apiUrl: string;

    trips: Trip[] = [];
    myTrips: Trip[] = [];
    currentTrip: Trip = null;

    constructor(private http: HttpClient,
                private router: Router,
                @Inject(APP_CONFIG) config: AppConfig) {
        this.apiUrl = config.apiUrl;
    }

    getAll() {
        return this.http.get<Trip[]>(`${this.apiUrl}/trips`);
    }

    getById(id: string) {
        return this.http.get<Trip>(`${this.apiUrl}/trips/${id}`);
    }

    getAllByUserId(id: string) {
        return this.http.get<Trip[]>(`${this.apiUrl}/trips/user/${id}`);
    }

    create(trip: Trip) {
        return this.http.post(`${this.apiUrl}/trips`, trip);
    }

    update(trip: Trip) {
        return this.http.put(`${this.apiUrl}/trips/${trip._id}`, trip);
    }

    delete(id: string) {
        return this.http.delete(`${this.apiUrl}/trips/${id}`);
    }

    loadAllTrips(userId) {
        this.getAllByUserId(userId).pipe(first()).subscribe(trips => {
            this.trips = trips;
            this.myTrips = trips;
        });
    }

    selectCurrentTrip(trip: Trip) {
        this.currentTrip = trip;
        this.router.navigate(['trip', trip._id]);
    }

    updateTrip(tripId: string) {
        this.router.navigate(['trip', tripId]);
    }

}
