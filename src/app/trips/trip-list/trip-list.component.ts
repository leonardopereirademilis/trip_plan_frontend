import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Trip} from '../trip/trip';
import {first} from 'rxjs/operators/first';
import {TripService} from '../trip.service';
import {NgbDateParserFormatter, NgbDatepickerI18n, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {I18n, NgbDatepickerI18nBR} from '../../_helpers/ngb-datepicker-i18n-br';
import {NgbDateBRParserFormatter} from '../../_helpers/ngb-date-br-parser-formatter';

@Component({
    selector: 'app-trip-list',
    templateUrl: './trip-list.component.html',
    styleUrls: ['./trip-list.component.css'],
    providers: [
        I18n,
        {provide: NgbDatepickerI18n, useClass: NgbDatepickerI18nBR},
        {provide: NgbDateParserFormatter, useClass: NgbDateBRParserFormatter}
    ]
})
export class TripListComponent implements OnInit {
    @Input() trip: Trip;
    @Input() index: number;
    @Input() trips: Trip[] = [];
    @Output() loadAllTrips = new EventEmitter();
    registerForm: FormGroup;
    today: NgbDateStruct;
    tomorrow: NgbDateStruct;
    minDate: NgbDateStruct;
    submitted = false;
    updateDate = true;
    internalError: string = null;
    updating = false;
    editable = false;
    tripOld: Trip;

    constructor(private router: Router,
                private tripService: TripService,
                private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        const now = new Date();
        this.today = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
        this.tomorrow = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() + 1};
        if (this.trip.tripDateStart) {
            this.minDate = {
                year: new Date(this.trip.tripDateStart).getFullYear(),
                month: new Date(this.trip.tripDateStart).getMonth() + 1,
                day: new Date(this.trip.tripDateStart).getDate()
            };
        }

        this.registerForm = this.formBuilder.group({
            tripName: ['', Validators.required],
            tripDateStart: [],
            tripDateEnd: []
        });

        this.updateForm();
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.registerForm.controls;
    }

    updateForm() {
        this.registerForm.get('tripName').setValue(this.trip.tripName);

        let dateStart: NgbDateStruct = null;
        if (this.trip.tripDateStart) {
            dateStart = {
                year: new Date(this.trip.tripDateStart).getFullYear(),
                month: new Date(this.trip.tripDateStart).getMonth() + 1,
                day: new Date(this.trip.tripDateStart).getDate()
            };
        }
        this.registerForm.get('tripDateStart').setValue(dateStart);

        let dateEnd: NgbDateStruct = null;
        if (this.trip.tripDateEnd) {
            dateEnd = {
                year: new Date(this.trip.tripDateEnd).getFullYear(),
                month: new Date(this.trip.tripDateEnd).getMonth() + 1,
                day: new Date(this.trip.tripDateEnd).getDate()
            };
        }
        this.registerForm.get('tripDateEnd').setValue(dateEnd);
    }

    updateTripDate(): void {
        if (this.updateDate) {
            if (this.registerForm.value.tripDateStart) {
                this.trip.tripDateStart = new Date(this.registerForm.value.tripDateStart.year, this.registerForm.value.tripDateStart.month - 1, this.registerForm.value.tripDateStart.day);
                this.minDate = this.registerForm.value.tripDateStart;
            }
            if (this.registerForm.value.tripDateEnd) {
                this.trip.tripDateEnd = new Date(this.registerForm.value.tripDateEnd.year, this.registerForm.value.tripDateEnd.month - 1, this.registerForm.value.tripDateEnd.day);
            }
            if (this.trip.tripDateStart > this.trip.tripDateEnd) {
                this.trip.tripDateEnd = this.trip.tripDateStart;
                this.trip.tripDateEnd.setDate(this.trip.tripDateEnd.getDate() + 1);

                const dateEnd: NgbDateStruct = {
                    year: new Date(this.trip.tripDateEnd).getFullYear(),
                    month: new Date(this.trip.tripDateEnd).getMonth() + 1,
                    day: new Date(this.trip.tripDateEnd).getDate()
                };

                this.updateDate = false;
                this.registerForm.get('tripDateEnd').setValue(dateEnd);
                this.trip.tripDateEnd = new Date(dateEnd.year, dateEnd.month - 1, dateEnd.day);
            }
        } else {
            this.updateDate = true;
        }
    }

    updateTrip() {
        this.updating = true;
        this.internalError = null;
        this.submitted = true;

        if (this.trips.find(trip => trip.tripName === this.registerForm.value.tripName && trip._id !== this.trip._id)) {
            this.registerForm.get('tripName').setErrors({'duplicated': true});
        }

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            this.updating = false;
            return;
        }

        this.trip.tripName = this.registerForm.value.tripName;

        this.tripService.update(this.trip).pipe(first()).subscribe(() => {
            this.submitted = false;
            this.updating = false;
            this.editable = false;
        }, err => {
            this.internalError = err;
        });
    }

    deleteTrip(id: string) {
        this.tripService.delete(id).pipe(first()).subscribe(() => {
            this.loadAllTrips.emit();
        });
    }

    startEditing() {
        this.tripOld = new Trip();
        this.editable = true;
        Object.assign(this.tripOld, this.trip);
    }

    cancelEditing() {
        Object.assign(this.trip, this.trip);
        this.updateForm();
        this.submitted = false;
        this.editable = false;
    }

}
