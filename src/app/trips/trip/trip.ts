﻿import {User} from '../../users/user';

export class Trip {
  id: string;
  _id: string;
  tripName: string;
  user: User;
  tripDateStart: Date;
  tripDateEnd: Date;
}
