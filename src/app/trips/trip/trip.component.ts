import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {TripService} from '../trip.service';
import {first} from 'rxjs/operators/first';
import {ItemsComponent} from './items/items.component';
import {NoteComponent} from './note/note.component';
import {Trip} from './trip';
import {Item} from './items/item';
import {isEmpty} from 'lodash';
import {ItemService} from './items/item.service';

@Component({
    selector: 'app-trip',
    templateUrl: './trip.component.html',
    styleUrls: ['./trip.component.css']
})
export class TripComponent implements OnInit {
    @Input() demo: string = null;
    @Input() currentTrip: Trip = null;
    @Input() allItems: Item[] = [];

    @ViewChild('tripItems') tripItems: ItemsComponent;
    @ViewChild('budgetItems') budgetItems: ItemsComponent;
    @ViewChild('flightItems') flightItems: ItemsComponent;
    @ViewChild('accommodationItems') accommodationItems: ItemsComponent;
    @ViewChild('carItems') carItems: ItemsComponent;
    @ViewChild('ticketItems') ticketItems: ItemsComponent;
    @ViewChild('insuranceItems') insuranceItems: ItemsComponent;
    @ViewChild('exchangeItems') exchangeItems: ItemsComponent;
    @ViewChild('otherItems') otherItems: ItemsComponent;
    @ViewChild('note') note: NoteComponent;

    constructor(private activatedRoute: ActivatedRoute,
                private router: Router,
                private itemService: ItemService,
                private tripService: TripService) {

        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.loadCurrentTrip();
            }
        });
    }

    ngOnInit(): void {
        this.loadCurrentTrip();
    }

    deleteTrip(id: string) {
        this.tripService.delete(id).pipe(first()).subscribe(() => {
            this.loadCurrentTrip();
        });
    }

    loadCurrentTrip() {
        const id = this.activatedRoute.snapshot.paramMap.get('id');
        if (id) {
            this.tripService.getById(id).pipe(first()).subscribe(trip => {
                this.tripService.currentTrip = trip;
                this.currentTrip = this.tripService.currentTrip;
                setTimeout(() => {
                    this.reloadCards();
                }, 100);
            });
        }
    }

    reloadCards() {
        this.tripItems.ngOnInit();
        this.budgetItems.ngOnInit();
        this.flightItems.ngOnInit();
        this.accommodationItems.ngOnInit();
        this.carItems.ngOnInit();
        this.ticketItems.ngOnInit();
        this.insuranceItems.ngOnInit();
        this.exchangeItems.ngOnInit();
        this.otherItems.ngOnInit();
        this.note.ngOnInit();
    }
}
