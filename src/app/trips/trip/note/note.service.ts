import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {APP_CONFIG, AppConfig} from '../../../app.config';
import {Note} from '../note/note';

@Injectable({
  providedIn: 'root'
})
export class NoteService {
  apiUrl: string;

  constructor(private http: HttpClient, @Inject(APP_CONFIG) config: AppConfig) {
    this.apiUrl = config.apiUrl;
  }

  getAll() {
    return this.http.get<Note[]>(`${this.apiUrl}/notes`);
  }

  getById(id: string) {
    return this.http.get<Note>(`${this.apiUrl}/notes/${id}`);
  }

  getByTripId(id: string) {
    return this.http.get<Note>(`${this.apiUrl}/notes/trip/${id}`);
  }

  create(note: Note) {
    return this.http.post(`${this.apiUrl}/notes`, note);
  }

  update(note: Note) {
    return this.http.put(`${this.apiUrl}/notes/${note._id}`, note);
  }

  delete(id: string) {
    return this.http.delete(`${this.apiUrl}/notes/${id}`);
  }
}
