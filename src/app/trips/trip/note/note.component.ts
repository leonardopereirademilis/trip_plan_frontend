import {Component, Inject, Input, OnInit, PLATFORM_ID} from '@angular/core';
import {first} from 'rxjs/operators/first';
import {DemoService} from '../../../_helpers/demo-service';
import {TripService} from '../../trip.service';
import {Note} from './note';
import {NoteService} from './note.service';
import {isPlatformBrowser} from '@angular/common';

@Component({
    selector: 'app-note',
    templateUrl: './note.component.html',
    styleUrls: ['./note.component.css']
})
export class NoteComponent implements OnInit {
    @Input() demo: string = null;
    note: Note = new Note();
    internalError: string = null;
    updating = false;

    constructor(private noteService: NoteService,
                private tripService: TripService,
                private demoService: DemoService,
                @Inject(PLATFORM_ID) private platformId: any) {
    }

    ngOnInit() {
        if (this.demo) {
            this.note = new Note();
            this.note.trip = this.demoService.currentDemoTrip;
            this.note.note = this.demoService.demoNote;
        } else {
            this.noteService.getByTripId(this.tripService.currentTrip._id).subscribe(note => {
                this.note = note;
            }, err => {
                this.note = new Note();
                this.note.trip = this.tripService.currentTrip;
            });
        }
    }

    saveNote() {
        this.updating = true;
        if (this.note._id) {
            this.noteService.update(this.note).pipe(first()).subscribe(() => {
                this.updating = false;
            }, err => {
                this.internalError = err;
            });
        } else {
            this.noteService.create(this.note).pipe(first()).subscribe(() => {
                this.noteService.getByTripId(this.tripService.currentTrip._id).subscribe(note => {
                    this.note = note;
                }, err => {
                    this.internalError = err;
                });
            }, err => {
                this.internalError = err;
            });
        }

    }

    reloadCards() {
        if (isPlatformBrowser(this.platformId)) {
            const classArr: any = document.querySelectorAll('.collapseable');
            classArr.forEach(element => {
                element.style.display = 'block';
                element.classList.remove('collapsed-card');
            });
        }
    }

}
