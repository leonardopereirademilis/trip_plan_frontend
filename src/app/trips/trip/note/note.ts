﻿import {Trip} from '../trip';

export class Note {
  id: string;
  _id: string;
  trip: Trip;
  note: string;
}
