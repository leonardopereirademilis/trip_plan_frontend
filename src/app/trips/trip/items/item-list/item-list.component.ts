import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators/first';
import {Item} from '../item';
import {ItemService} from '../item.service';

@Component({
    selector: 'app-item-list',
    templateUrl: './item-list.component.html',
    styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {
    @Input() item: Item;
    @Input() index: number;
    @Input() items: Item[] = [];
    @Output() loadAllItems = new EventEmitter();
    registerForm: FormGroup;
    submitted = false;
    internalError: string = null;
    updating = false;
    editable = false;
    itemOld: Item;

    currencyIndex: number;
    currencyList = ['Dolar', 'Euro', 'Pound', 'Peso', 'Other'];
    currencySymbolList = ['$', '€', '£', '$', '$'];

    constructor(private router: Router,
                private itemService: ItemService,
                private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            itemName: ['', Validators.required],
            value: ['', Validators.required],
            exchangeValue: [''],
            times: ['', [Validators.required, Validators.min(1)]],
            itemOk: [],
            selectedCurrency: []
        });

        this.updateForm();
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.registerForm.controls;
    }

    updateForm() {
        this.registerForm.get('itemName').setValue(this.item.itemName);
        this.registerForm.get('value').setValue(this.item.value);
        this.registerForm.get('exchangeValue').setValue(this.item.exchangeValue);
        this.registerForm.get('times').setValue(this.item.times);
        this.registerForm.get('itemOk').setValue(this.item.itemOk);
        this.registerForm.get('selectedCurrency').setValue(this.item.selectedCurrency);

        if (this.item.itemType === 'exchange') {
            this.selectCurrency();
        }
    }

    updateItem() {
        if (this.registerForm.dirty) {
            this.updating = true;
            this.internalError = null;
            this.submitted = true;

            // if (this.items.find(item => item.itemName === this.registerForm.value.itemName && item._id !== this.item._id)) {
            //   this.registerForm.get('itemName').setErrors({'duplicated': true});
            // }

            if (this.item.itemType === 'exchange' && !this.registerForm.get('exchangeValue').value) {
                this.registerForm.get('exchangeValue').setErrors({'required': true});
            }

            // stop here if form is invalid
            if (this.registerForm.invalid || (!this.registerForm.get('itemOk').value && !this.item.itemDate)) {
                this.updating = false;
                return;
            }

            this.item.itemName = this.registerForm.value.itemName;
            this.item.value = this.registerForm.value.value;
            this.item.exchangeValue = this.registerForm.value.exchangeValue;
            this.item.selectedCurrency = this.registerForm.value.selectedCurrency;
            this.item.times = this.registerForm.value.times;
            this.item.itemOk = this.registerForm.value.itemOk ? true : false;

            this.itemService.update(this.item).pipe(first()).subscribe(() => {
                this.submitted = false;
                this.updating = false;
                this.editable = false;
                this.loadAllItems.emit();
            }, err => {
                this.internalError = err;
            });
        }
    }

    deleteItem(id: string) {
        this.itemService.delete(id).pipe(first()).subscribe(() => {
            this.loadAllItems.emit();
        });
    }

    selectCurrency() {
        this.currencyIndex = this.currencyList.indexOf(this.registerForm.get('selectedCurrency').value);
        if (this.currencyIndex === -1) {
            this.currencyIndex = null;
        }
        const itemName = !this.registerForm.get('selectedCurrency').value ? '' : this.currencyList[this.currencyIndex] + ' (' + this.currencySymbolList[this.currencyIndex] + ')';
        this.registerForm.get('itemName').setValue(itemName);
    }

    startEditing() {
        this.itemOld = new Item();
        this.editable = true;
        Object.assign(this.itemOld, this.item);
    }

    cancelEditing() {
        Object.assign(this.item, this.itemOld);
        this.updateForm();
        this.submitted = false;
        this.editable = false;
    }
}
