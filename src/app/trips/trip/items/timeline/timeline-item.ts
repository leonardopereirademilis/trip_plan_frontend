﻿export class TimelineItem {
  title: string;
  description: string;
  date: Date;
  value: number;
  timelineItemOk: boolean;

  constructor() {
  }
}
