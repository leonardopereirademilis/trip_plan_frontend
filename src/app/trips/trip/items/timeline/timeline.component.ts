import {Component, Input, OnInit} from '@angular/core';
import {TimelineItem} from './timeline-item';
import {Item} from '../item';

@Component({
    selector: 'app-timeline',
    templateUrl: './timeline.component.html',
    styleUrls: ['./timeline.component.css']
})
export class TimelineComponent implements OnInit {
    @Input() itemLabel: string;
    @Input() itemType: string;
    @Input() demo: string = null;

    timelineItems: TimelineItem[] = [];
    currentMonth: number;

    private _items: Item[];

    @Input()
    get items(): Item[] {
        return this._items;
    }

    set items(value: Item[]) {
        this._items = value;
        this.mountTimelineItems();
    }

    constructor() {
        this.currentMonth = new Date().getMonth();
    }

    ngOnInit() {
    }

    mountTimelineItems() {
        this.timelineItems = [];
        this.items.forEach(item => {
            for (let i = 0; i < item.times; i++) {
                let timelineItemNew = new TimelineItem();
                timelineItemNew.title = '';
                timelineItemNew.description = '';
                timelineItemNew.value = 0;

                let timelineItemOld = null;

                if (item.itemDate && !item.itemOk) {
                    const date = new Date(item.itemDate);
                    date.setMonth(date.getMonth() + i);
                    timelineItemOld = this.timelineItems.find(ti => ti.date && ti.date.getMonth() === date.getMonth() && ti.date.getFullYear() === date.getFullYear());
                } else {
                    timelineItemOld = this.timelineItems.find(ti => !ti.date);
                }

                if (timelineItemOld) {
                    timelineItemNew = timelineItemOld;
                }

                const portion = Math.round((item.value / item.times) * 100) / 100;
                timelineItemNew.title = 'Trip items';
                const times = item.times > 1 ? ((i + 1) + '/' + item.times) : '1x';
                let portionString = portion.toLocaleString('pt-BR');
                if (portionString.indexOf(',') === -1) {
                    portionString += ',00';
                }
                timelineItemNew.value += portion;
                timelineItemNew.timelineItemOk = item.itemOk;
                if (item.itemDate) {
                    timelineItemNew.date = new Date(item.itemDate);
                    timelineItemNew.date.setMonth(timelineItemNew.date.getMonth() + i);
                    if (timelineItemNew.date < new Date()) {
                        timelineItemNew.timelineItemOk = true;
                    }
                }

                timelineItemNew.description += '<li>' + item.itemName +
                    '<span class="times">(' + times + ')</span>' +
                    (timelineItemNew.timelineItemOk ? '<span class=" value text-success"><i class="fa fa-caret-up"></i>' : '<span class="value text-danger"><i class="fa fa-caret-down"></i>') +
                    ' R$ ' + portionString + '</span>' +
                    '</li>';

                if (!timelineItemOld) {
                    this.timelineItems.push(timelineItemNew);
                }
            }
        });
    }
}
