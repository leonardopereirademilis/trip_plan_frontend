import {Component, EventEmitter, Input, OnInit, Output, ViewChild, ElementRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {first} from 'rxjs/operators/first';
import {Item} from '../item';
import {ItemService} from '../item.service';
import {TripService} from '../../../trip.service';

@Component({
    selector: 'app-new-item',
    templateUrl: './new-item.component.html',
    styleUrls: ['./new-item.component.css']
})
export class NewItemComponent implements OnInit {
    registerForm: FormGroup;
    newItem: Item = new Item();
    submitted = false;
    internalError: string = null;

    @Input() itemType: string;
    @Output() loadAllItems = new EventEmitter();

    @ViewChild('content') content: ElementRef;

    currencyIndex: number;
    currencyList = ['Dolar', 'Euro', 'Pound', 'Peso', 'Other'];
    currencySymbolList = ['$', '€', '£', '$', '$'];

    constructor(private formBuilder: FormBuilder,
                private modalService: NgbModal,
                private tripService: TripService,
                private itemService: ItemService) {
    }

    ngOnInit() {
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.registerForm.controls;
    }

    open(content: any) {
        this.modalService.dismissAll();
        this.submitted = false;
        this.internalError = null;
        this.newItem = new Item();
        this.newItem.trip = this.tripService.currentTrip;
        this.newItem.itemType = this.itemType;

        this.registerForm = this.formBuilder.group({
            itemName: ['', Validators.required],
            value: ['', Validators.required],
            times: ['', [Validators.required, Validators.min(1)]],
            exchangeValue: [''],
            itemOk: [],
            selectedCurrency: []
        });

        this.registerForm.get('times').setValue(1);

        this.modalService.open(content, {size: 'lg', centered: true}).result.then((result) => {
            this.newItem = new Item();
            this.loadAllItems.emit();
        }, (reason) => {
            this.newItem = new Item();
            this.registerForm.get('itemName').setValue(null);
            this.registerForm.get('value').setValue(null);
            this.registerForm.get('exchangeValue').setValue(null);
            this.registerForm.get('times').setValue(null);
            this.registerForm.get('itemOk').setValue(null);
            this.registerForm.get('selectedCurrency').setValue(null);
            this.submitted = false;
        });
    }

    onSubmit(modal: NgbActiveModal) {
        this.internalError = null;
        this.submitted = true;

        if (this.itemType === 'exchange' && !this.registerForm.get('exchangeValue').value) {
            this.registerForm.get('exchangeValue').setErrors({'required': true});
        }

        // stop here if form is invalid
        if (this.registerForm.invalid || (!this.registerForm.get('itemOk').value && !this.newItem.itemDate)) {
            return;
        }

        this.newItem.trip = this.tripService.currentTrip;
        this.newItem.itemType = this.itemType;
        this.newItem.itemName = this.registerForm.value.itemName;
        this.newItem.value = this.registerForm.value.value;
        this.newItem.exchangeValue = this.registerForm.value.exchangeValue;
        this.newItem.selectedCurrency = this.registerForm.value.selectedCurrency;
        this.newItem.times = this.registerForm.value.times;
        this.newItem.itemOk = this.registerForm.value.itemOk ? true : false;

        this.itemService.create(this.newItem).pipe(first()).subscribe(() => {
            modal.close();
        }, err => {
            this.internalError = err;
        });
    }

    selectCurrency() {
        this.currencyIndex = this.currencyList.indexOf(this.registerForm.get('selectedCurrency').value);
        if (this.currencyIndex === -1) {
            this.currencyIndex = null;
        }
        const itemName = !this.registerForm.get('selectedCurrency').value ? '' : this.currencyList[this.currencyIndex] + ' (' + this.currencySymbolList[this.currencyIndex] + ')';
        this.registerForm.get('itemName').setValue(itemName);
    }
}
