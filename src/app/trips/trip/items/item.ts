﻿import {Trip} from '../trip';

export class Item {
  id: string;
  _id: string;
  itemType: string;
  itemName: string;
  trip: Trip;
  value: number;
  exchangeValue: number;
  times: number;
  itemDate: Date;
  itemOk: boolean;
  selectedCurrency: string;
}
