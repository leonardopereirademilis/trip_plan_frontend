import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {APP_CONFIG, AppConfig} from '../../../app.config';
import {Item} from './item';

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  apiUrl: string;

  constructor(private http: HttpClient, @Inject(APP_CONFIG) config: AppConfig) {
    this.apiUrl = config.apiUrl;
  }

  getAll() {
    return this.http.get<Item[]>(`${this.apiUrl}/items`);
  }

  getById(id: string) {
    return this.http.get<Item>(`${this.apiUrl}/items/${id}`);
  }

  getAllByTripId(id: string) {
    return this.http.get<Item[]>(`${this.apiUrl}/items/trip/${id}`);
  }

  getAllByTripIdAndItemType(id: string, itemType: string) {
    return this.http.get<Item[]>(`${this.apiUrl}/items/trip/${id}/${itemType}`);
  }

  create(item: Item) {
    return this.http.post(`${this.apiUrl}/items`, item);
  }

  update(item: Item) {
    return this.http.put(`${this.apiUrl}/items/${item._id}`, item);
  }

  delete(id: string) {
    return this.http.delete(`${this.apiUrl}/items/${id}`);
  }
}
