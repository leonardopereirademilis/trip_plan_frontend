import {Component, EventEmitter, Inject, Input, OnInit, Output, PLATFORM_ID, Renderer2} from '@angular/core';
import {first} from 'rxjs/operators/first';
import {DemoService} from '../../../_helpers/demo-service';
import {TripService} from '../../trip.service';
import {Item} from './item';
import {ItemService} from './item.service';
import {isPlatformBrowser} from '@angular/common';

@Component({
    selector: 'app-items',
    templateUrl: './items.component.html',
    styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
    @Input() itemType: string;
    @Input() itemLabel: string;
    @Input() demo: string = null;

    @Output() loadAllItemsTypesEmitter = new EventEmitter();

    @Input() allItems: Item[] = [];
    items: Item[] = [];
    budgetItems: Item[] = [];
    itemTotal: number;
    itemOk: number;
    itemCurrent: number;
    itemFuture: number;
    tripBudget: number;
    exchangeAverage: number;
    itemsValueTotal = [];
    itemsValuePayed = [];
    itemsValueCurrent = [];
    itemsValuePending = [];
    itemsIndex = [];

    maxDate: Date;
    minDate: Date;
    interval: number;
    dateInterval: Date[] = [];
    totals: number[] = [];

    timelineChartData: any;

    currentMonth: Date;

    loaded = false;

    constructor(private itemService: ItemService,
                private tripService: TripService,
                private demoService: DemoService,
                private render: Renderer2,
                @Inject(PLATFORM_ID) private platformId: any) {
    }

    ngOnInit() {
        this.currentMonth = new Date(new Date().getFullYear(), new Date().getMonth(), 1, 0, 0, 0, 0);
        this.loadAllItems();
        this.reloadCards();
    }

    loadAllItems() {
        this.items = [];
        this.budgetItems = [];
        if (this.demo) {
            this.buildItems(this.demoService.demoItems);
        } else {
            this.itemService.getAllByTripId(this.tripService.currentTrip._id).pipe(first()).subscribe(items => {
                if (items && items.length > 0) {
                    this.buildItems(items);
                }
            });
        }
    }

    loadAllItemTypes() {
        this.loadAllItemsTypesEmitter.emit();
    }

    private buildItems(items) {
        items.forEach(item => {
            item.itemDate = new Date(item.itemDate);
            if (!this.minDate || item.itemDate < this.minDate) {
                this.minDate = item.itemDate;
            }
            if (!this.maxDate || item.itemDate > this.maxDate) {
                this.maxDate = item.itemDate;
            }
            const finalDate = new Date(item.itemDate);
            finalDate.setMonth(finalDate.getMonth() + (item.times - 1));
            if (!this.maxDate || finalDate > this.maxDate) {
                this.maxDate = finalDate;
            }
        });

        this.interval = (this.maxDate.getFullYear() - this.minDate.getFullYear()) * 12;
        this.interval -= this.minDate.getMonth() + 1;
        this.interval += this.maxDate.getMonth();
        this.interval = this.interval <= 0 ? 0 : this.interval;

        this.dateInterval = [];

        this.dateInterval.push(this.minDate);
        for (let i = 0; i < this.interval; i++) {
            const date = new Date(this.minDate);
            date.setMonth(date.getMonth() + (i + 1));
            this.dateInterval.push(date);
        }

        if (this.minDate.getTime() !== this.maxDate.getTime()) {
            this.dateInterval.push(this.maxDate);
        }
        this.allItems = items;
        this.items = items;

        this.listItems();

        if (this.itemType === 'trip') {
            this.budgetItems = this.sortItemsByOk(
                this.sortItemsByDate(
                    this.allItems.filter(item => item.itemType === 'budget')
                )
            );
            this.items = this.sortItemsByOk(
                this.sortItemsByDate(
                    this.allItems.filter(item => item.itemType !== 'budget')
                )
            );
        } else {
            this.items = this.sortItemsByOk(
                this.sortItemsByDate(
                    this.allItems.filter(item => item.itemType === this.itemType)
                )
            );
        }

        this.setTotal();
    }

    sortItemsByDate(items: Item[]): Item[] {
        return items.sort((item1, item2) => {
            if (item1.itemDate > item2.itemDate) {
                return 1;
            }
            if (item1.itemDate < item2.itemDate) {
                return -1;
            }
            return 0;
        });
    }

    sortItemsByOk(items: Item[]): Item[] {
        return items.sort((item1, item2) => {
            if (item1.itemDate && (!item2.itemDate || item2.itemOk)) {
                return 1;
            }
            if ((!item1.itemDate || item1.itemOk) && item2.itemDate) {
                return -1;
            }
            return 0;
        });
    }

    setTotal() {
        this.itemTotal = 0;
        this.itemOk = 0;
        this.itemCurrent = 0;
        this.itemFuture = 0;
        this.tripBudget = 0;
        this.exchangeAverage = 0;

        this.itemsValuePayed = Array(this.itemsIndex.length).fill(0);
        this.itemsValueCurrent = Array(this.itemsIndex.length).fill(0);
        this.itemsValuePending = Array(this.itemsIndex.length).fill(0);

        this.budgetItems.forEach(budget => {
            this.tripBudget += budget.value;
        });

        let itemsExchangeValue = 0;
        this.items.forEach(item => {
            this.itemTotal += item.value;
            itemsExchangeValue += item.exchangeValue;

            for (let i = 0; i < item.times; i++) {
                const itemDate = new Date(item.itemDate);
                itemDate.setMonth(itemDate.getMonth() + i);
                if (item.itemOk || !item.itemDate || itemDate.getTime() < this.currentMonth.getTime()) {
                    this.itemOk += item.value / item.times;
                    this.itemsValuePayed[this.itemsIndex.indexOf(item.itemType)] += item.value / item.times;
                } else if (itemDate.getTime() === this.currentMonth.getTime()) {
                    this.itemCurrent += item.value / item.times;
                    this.itemsValueCurrent[this.itemsIndex.indexOf(item.itemType)] += item.value / item.times;
                } else {
                    this.itemFuture += item.value / item.times;
                    this.itemsValuePending[this.itemsIndex.indexOf(item.itemType)] += item.value / item.times;
                }
            }
        });

        this.dateInterval.forEach(date => {
            let total = 0;
            this.items.forEach(item => {
                for (let i = 0; i < item.times; i++) {
                    const itemDate = new Date(item.itemDate);
                    itemDate.setMonth(itemDate.getMonth() + i);
                    if (itemDate.getTime() === date.getTime()) {
                        total += item.value / item.times;
                    }
                }
            });
            this.totals.push(total);
        });

        this.exchangeAverage = this.itemTotal / itemsExchangeValue;

        this.createTimeline();

        setTimeout(() => {
            this.loaded = true;
        }, 100);

    }

    listItems() {
        this.itemsValueTotal = [];
        this.itemsIndex = [];
        this.items.forEach(item => {
            if (item.itemType !== 'budget') {
                if (this.itemsValueTotal[item.itemType]) {
                    this.itemsValueTotal[item.itemType] += item.value;
                } else {
                    this.itemsValueTotal[item.itemType] = item.value;
                    this.itemsIndex.push(item.itemType);
                }
            }
        });

        this.itemsIndex = this.sortIndexByValue(this.itemsIndex);
        this.sortArrays();
    }

    sortIndexByValue(index: any[]): any[] {
        return index.sort((index1, index2) => {
            if (this.itemsValueTotal[index1] < this.itemsValueTotal[index2]) {
                return 1;
            }
            if (this.itemsValueTotal[index1] > this.itemsValueTotal[index2]) {
                return -1;
            }
            return 0;
        });
    }

    capitalize(myString: string): string {
        return myString.charAt(0).toUpperCase() + myString.slice(1);
    }

    sortArrays() {
        const sortedItemsValueTotal = [];
        const sortedItemsValuePayed = [];
        const sortedItemsValueCurrent = [];
        const sortedItemsValuePending = [];
        this.itemsIndex.forEach(index => {
            sortedItemsValueTotal.push(this.itemsValueTotal[index]);
            sortedItemsValuePayed.push(this.itemsValuePayed[index]);
            sortedItemsValueCurrent.push(this.itemsValueCurrent[index]);
            sortedItemsValuePending.push(this.itemsValuePending[index]);
        });

        this.itemsValueTotal = sortedItemsValueTotal;
        this.itemsValuePayed = sortedItemsValuePayed;
        this.itemsValueCurrent = sortedItemsValueCurrent;
        this.itemsValuePending = sortedItemsValuePending;
    }

    reloadCards() {
        if (isPlatformBrowser(this.platformId)) {
            const classArr: any = document.querySelectorAll('.collapseable');
            classArr.forEach(element => {
                element.style.display = 'block';
                element.classList.remove('collapsed-card');
            });
        }
    }

    createTimeline() {
        const dataTable = [];
        let gridLines = 0;

        dataTable.push(['Name', 'From', 'To']);
        this.items.forEach(item => {
            if (item.itemDate instanceof Date && !isNaN(item.itemDate.getTime())) {
                const dateStart = new Date(item.itemDate);
                const dateEnd = new Date(item.itemDate);
                dateEnd.setMonth(dateEnd.getMonth() + item.times);
                dataTable.push([item.itemName, dateStart, dateEnd]);
            }

            if (item.times > gridLines) {
                gridLines = item.times;
            }
        });

        this.timelineChartData = {
            chartType: 'Timeline',
            dataTable: dataTable,
            options: {
                hAxis: {
                    format: 'MM/yyyy',
                    gridlines: {count: gridLines},
                }
            }
        };
    }

    getItemType(item: Item, date: Date): string {
        for (let i = 0; i < item.times; i++) {
            const itemDate = new Date(item.itemDate);
            itemDate.setMonth(itemDate.getMonth() + i);

            if (itemDate.getMonth() === date.getMonth() && itemDate.getFullYear() === date.getFullYear()) {
                if (date.getTime() < this.currentMonth.getTime()) {
                    return 'item-success';
                }

                if (date.getTime() === this.currentMonth.getTime()) {
                    return 'item-warning';
                }
                return 'item-danger';
            }
        }

        if (item.itemDate instanceof Date && isNaN(item.itemDate.getTime()) && item.itemOk && date.getTime() === this.minDate.getTime()) {
            return 'item-success';
        }

        return '';
    }

    arredondar(numero: number): number {
        return Math.round(numero);
    }
}
