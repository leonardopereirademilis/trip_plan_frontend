import {Component, Input, OnInit} from '@angular/core';
import {TripService} from './trip.service';
import {UserService} from '../users/user.service';
import {DemoService} from '../_helpers/demo-service';
import {User} from '../users/user';
import {Trip} from './trip/trip';
import {first} from 'rxjs/internal/operators';

@Component({
    selector: 'app-trips',
    templateUrl: './trips.component.html',
    styleUrls: ['./trips.component.css']
})
export class TripsComponent implements OnInit {
    @Input() demo = false;
    currentUser: User;
    trips: Trip[];

    constructor(private userService: UserService,
                private tripService: TripService,
                private demoService: DemoService) {
    }

    ngOnInit(): void {
        this.currentUser = this.userService.currentUser;
        if (this.demo) {
            this.trips = this.demoService.demoTrips;
        } else {
            this.loadAllTrips();
        }
    }

    loadAllTrips() {
        this.tripService.getAllByUserId(this.currentUser._id).pipe(first()).subscribe(trips => {
            this.trips = trips;
        });
    }

}
