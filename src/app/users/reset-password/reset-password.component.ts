import {Component, OnInit} from '@angular/core';
import {first} from 'rxjs/operators/first';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../user.service';
import {AlertService} from '../../_alert/alert.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  resetPasswordForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.resetPasswordForm = this.formBuilder.group({
      token: ['', [Validators.required]],
      newPassword: ['', [Validators.required, Validators.minLength(6)]],
      verifyPassword: ['', [Validators.required, Validators.minLength(6)]]
    });


    const token = document.location.href.split('token=')[1];
    this.resetPasswordForm.get('token').setValue(token);
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.resetPasswordForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.f.newPassword.value !== this.f.verifyPassword.value) {
      this.resetPasswordForm.get('newPassword').setErrors({'noMatch': true});
      this.resetPasswordForm.get('verifyPassword').setErrors({'noMatch': true});
    }

    // stop here if form is invalid
    if (this.resetPasswordForm.invalid) {
      return;
    }

    this.loading = true;
    this.userService.resetPassword({
      'newPassword': this.f.newPassword.value,
      'verifyPassword': this.f.verifyPassword.value,
      'token': this.f.token.value
    })
      .pipe(first())
      .subscribe(
        data => {
          this.loading = false;
          this.alertService.success('Password updated', true);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

}
