import {Component, OnInit} from '@angular/core';
import {UserService} from '../user.service';
import {first} from 'rxjs/operators/first';
import {AlertService} from '../../_alert/alert.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.forgotPasswordForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.forgotPasswordForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.forgotPasswordForm.invalid) {
      return;
    }

    this.loading = true;
    this.userService.forgotPassword({'username': this.f.username.value})
      .pipe(first())
      .subscribe(
        data => {
          this.loading = false;
          this.alertService.success('E-mail instructions', true);
        },
        error => {
          this.loading = false;
          this.alertService.error(error);
        });
  }

}
