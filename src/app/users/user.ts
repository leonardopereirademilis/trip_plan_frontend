﻿export class User {
  id: number;
  _id: string;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  token: string;
}

