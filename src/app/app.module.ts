import {registerLocaleData} from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import localePt from '@angular/common/locales/pt';
import {LOCALE_ID, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TransferHttpCacheModule} from '@nguniversal/common';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {ConfirmationPopoverModule} from 'angular-confirmation-popover';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {QuillModule} from 'ngx-quill';
import {environment} from '../environments/environment';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {APP_CONFIG} from './app.config';
import {HomeComponent} from './home/home.component';
import {ContactComponent} from './contact/contact.component';
import {NewTripComponent} from './trips/new-trip/new-trip.component';
import {TripListComponent} from './trips/trip-list/trip-list.component';
import {ItemListComponent} from './trips/trip/items/item-list/item-list.component';
import {ItemsComponent} from './trips/trip/items/items.component';
import {NewItemComponent} from './trips/trip/items/new-item/new-item.component';
import {TimelineComponent} from './trips/trip/items/timeline/timeline.component';
import {NoteComponent} from './trips/trip/note/note.component';
import {TripComponent} from './trips/trip/trip.component';
import {TripsComponent} from './trips/trips.component';
import {ForgotPasswordComponent} from './users/forgot-password/forgot-password.component';
import {LoginComponent} from './users/login/login.component';
import {RegisterComponent} from './users/register/register.component';
import {ResetPasswordComponent} from './users/reset-password/reset-password.component';
import {AlertComponent} from './_alert/alert.component';
import {ErrorInterceptor} from './_helpers/error.interceptor';
import {JwtInterceptor} from './_helpers/jwt.interceptor';
import {MonthPickerComponent} from './_month-picker/month-picker.component';
import {NgxUiLoaderHttpModule, NgxUiLoaderModule, NgxUiLoaderRouterModule} from 'ngx-ui-loader';
import {ChartsModule} from 'ng2-charts';
import {ChartjsComponent} from './_chartjs/chartjs.component';
import {AdvertisementComponent} from './_advertisement/advertisement.component';
import {AboutComponent} from './about/about.component';

registerLocaleData(localePt, 'pt');

export const APP_ID = 'trip_plan_frontend';

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        AppComponent,
        TripComponent,
        HomeComponent,
        ContactComponent,
        AboutComponent,
        LoginComponent,
        RegisterComponent,
        AlertComponent,
        TripsComponent,
        NewTripComponent,
        TripListComponent,
        NewItemComponent,
        ItemListComponent,
        TimelineComponent,
        ItemsComponent,
        MonthPickerComponent,
        NoteComponent,
        ForgotPasswordComponent,
        ResetPasswordComponent,
        ChartjsComponent,
        AdvertisementComponent
    ],
    imports: [
        BrowserModule.withServerTransition({appId: APP_ID}),
        RouterModule.forRoot([
            {path: '', component: HomeComponent, pathMatch: 'full'},
            {path: 'lazy', loadChildren: './lazy/lazy.module#LazyModule'},
            {path: 'lazy/nested', loadChildren: './lazy/lazy.module#LazyModule'}
        ]),
        TransferHttpCacheModule,
        ReactiveFormsModule,
        HttpClientModule,
        AppRoutingModule,
        NgbModule,
        FormsModule,
        CurrencyMaskModule,
        QuillModule,
        NgxUiLoaderModule,
        NgxUiLoaderHttpModule,
        NgxUiLoaderRouterModule,
        ConfirmationPopoverModule.forRoot({
            confirmButtonType: 'danger' // set defaults here
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        ChartsModule
    ],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
        {provide: APP_CONFIG, useValue: environment.APP_CONFIG_IMPL},
        {provide: LOCALE_ID, useValue: 'pt'},
        {provide: 'LOCALSTORAGE', useFactory: getLocalStorage}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}

export function getLocalStorage() {
    return (typeof window !== 'undefined') ? window.localStorage : null;
}
