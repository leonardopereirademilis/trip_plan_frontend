import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './users/login/login.component';
import {RegisterComponent} from './users/register/register.component';
import {TripComponent} from './trips/trip/trip.component';
import {AuthGuard} from './_guards/auth.guard';
import {TripsComponent} from './trips/trips.component';
import {HomeComponent} from './home/home.component';
import {ContactComponent} from './contact/contact.component';
import {ForgotPasswordComponent} from './users/forgot-password/forgot-password.component';
import {ResetPasswordComponent} from './users/reset-password/reset-password.component';
import {AboutComponent} from './about/about.component';

const routes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'about', component: AboutComponent},
    {path: 'contact', component: ContactComponent},
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},
    {path: 'forgot-password', component: ForgotPasswordComponent},
    {path: 'reset-password', component: ResetPasswordComponent},
    {path: 'trips', component: TripsComponent, canActivate: [AuthGuard]},
    {path: 'trip/:id', component: TripComponent, canActivate: [AuthGuard]},
    {path: 'not-found', loadChildren: './_not-found/not-found.module#NotFoundModule'},
    {path: '**', redirectTo: 'not-found'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        initialNavigation: 'enabled',
    }),
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
