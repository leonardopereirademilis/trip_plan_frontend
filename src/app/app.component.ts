import {isPlatformBrowser} from '@angular/common';
import {Component, ElementRef, Inject, OnDestroy, OnInit, PLATFORM_ID, ViewChild} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {isEmpty} from 'lodash';
import {first} from 'rxjs/internal/operators';
import {TripService} from './trips/trip.service';
import {NewItemComponent} from './trips/trip/items/new-item/new-item.component';
import {AuthenticationService} from './users/authentication.service';
import {UserService} from './users/user.service';
import {DemoService} from './_helpers/demo-service';
import {Trip} from './trips/trip/trip';
import {User} from './users/user';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
    demo = false;
    currentUser: User;
    currentTrip: Trip;
    myTrips: Trip[];

    @ViewChild('tripsMenu') tripsMenu: ElementRef;
    @ViewChild('myTripsLink') myTripsLink: ElementRef;
    @ViewChild('newItem') newItem: NewItemComponent;

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute,
                private authenticationService: AuthenticationService,
                private userService: UserService,
                private translate: TranslateService,
                private tripService: TripService,
                private demoService: DemoService,
                @Inject(PLATFORM_ID) private platformId: any) {
        if (isPlatformBrowser(this.platformId)) {
            this.userService.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
                this.userService.currentUser = user;
                this.currentUser = this.userService.currentUser;
            });
        }
        translate.setDefaultLang('pt');

        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                const url = this.router.routerState.snapshot.url.split('/');
                if (url && url.length === 3 && url[1] === 'trip' && !isEmpty(url[2])) {
                    this.tripService.getById(url[2]).pipe(first()).subscribe(trip => {
                        this.tripService.currentTrip = trip;
                        this.currentTrip = this.tripService.currentTrip;
                        if (this.tripsMenu) {
                            this.tripsMenu.nativeElement.classList.add('menu-open');
                        }
                        if (this.myTripsLink) {
                            this.myTripsLink.nativeElement.classList.add('active');
                        }
                    });
                } else {
                    this.tripService.currentTrip = null;
                    this.currentTrip = null;
                    if (this.tripsMenu) {
                        this.tripsMenu.nativeElement.classList.remove('menu-open');
                    }
                    if (this.myTripsLink) {
                        this.myTripsLink.nativeElement.classList.remove('active');
                    }
                }

                if ((!this.myTrips || this.myTrips) && this.currentUser) {
                    this.tripService.getAllByUserId(this.currentUser._id).pipe(first()).subscribe(trips => {
                        this.myTrips = trips;
                    });
                }
            }
        });
    }

    ngOnInit() {
        this.tripService.currentTrip = null;
        this.currentTrip = null;
        if (this.demo) {
            this.tripService.trips = this.demoService.demoTrips;
            this.myTrips = [];
        } else if (this.userService.currentUser) {
            this.tripService.getAllByUserId(this.currentUser._id).pipe(first()).subscribe(trips => {
                this.myTrips = trips;
                this.tripService.trips = trips;
                this.tripService.myTrips = trips;
            });
        }
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        if (this.userService.currentUserSubscription) {
            this.userService.currentUserSubscription.unsubscribe();
        }
    }

    logout() {
        this.userService.currentUser = null;
        this.currentUser = null;
        this.authenticationService.logout();
        this.router.navigate(['/']);
    }

    copy() {
        const el = document.createElement('textarea');
        el.value = 'contato@gastosdeviagem.com.br';
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    }

    addItem(itemType: string) {
        this.newItem.itemType = itemType;
        this.newItem.open(this.newItem.content);
    }

    selectCurrentTrip(trip: Trip) {
        this.currentTrip = trip;
        this.tripService.selectCurrentTrip(trip);
    }

    loadAllItems() {
        const currentUrl = this.router.url + '?';
        this.router.navigateByUrl(currentUrl)
            .then(() => {
                this.router.navigated = false;
                this.router.navigate([this.router.url]);
            });
    }
}
