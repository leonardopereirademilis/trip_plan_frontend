import {Component, OnInit} from '@angular/core';
import {TripService} from '../trips/trip.service';
import {DemoService} from '../_helpers/demo-service';
import {Trip} from '../trips/trip/trip';
import {TranslateService} from '@ngx-translate/core';
import {Item} from '../trips/trip/items/item';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    loaded = false;
    trips: Trip[];
    currentTrip: Trip;
    allItems: Item[];

    constructor(private demoservice: DemoService,
                private tripService: TripService,
                private translate: TranslateService) {
    }

    ngOnInit(): void {
        this.tripService.currentTrip = null;
        let translatedTrips = 0;
        let translatedItems = 0;

        this.trips = this.demoservice.loadDemoTrips();
        this.currentTrip = this.demoservice.currentDemoTrip;
        this.allItems = this.demoservice.loadAllDemoItems();

        this.trips.forEach(trip => {
            this.translate.get(trip.tripName).subscribe(value => {
                trip.tripName = value;
                translatedTrips++;

                if (translatedTrips === this.trips.length && translatedItems === this.allItems.length) {
                    this.loaded = true;
                }
            });
        });

        this.allItems.forEach(item => {
            this.translate.get(item.itemName).subscribe(value => {
                item.itemName = value;
                translatedItems++;

                if (translatedTrips === this.trips.length && translatedItems === this.allItems.length) {
                    this.loaded = true;
                }
            });
        });

        this.demoservice.loadDemoNote();
    }
}
