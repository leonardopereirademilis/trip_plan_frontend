import 'zone.js/dist/zone-node';
import {enableProdMode, ValueProvider} from '@angular/core';
import {renderModuleFactory} from '@angular/platform-server';
import {join} from 'path';
import {readFileSync} from 'fs';
import {REQUEST, RESPONSE} from '@nguniversal/express-engine/tokens';
import {provideModuleMap} from '@nguniversal/module-map-ngfactory-loader';

const compression = require('compression');

const http = require('http');
// const https = require('https');

let DIST_FOLDER = '';
if (process.cwd().includes('trip_plan_frontend')) {
    DIST_FOLDER = join(process.cwd(), 'dist');
} else {
    DIST_FOLDER = join(process.cwd(), 'trip_plan_frontend', 'dist');
}

// const privateKey = readFileSync(join(DIST_FOLDER, 'browser', 'certificate', 'server.key'), 'utf8');
// const certificate = readFileSync(join(DIST_FOLDER, 'browser', 'certificate', 'server.cert'), 'utf8');

// const credentials = {key: privateKey, cert: certificate};
const express = require('express');

// Faster server renders w/ Prod mode (dev mode never needed)
enableProdMode();

// Express server
const app = express();

app.use(compression());

const PORT_HTTP = process.env.NODE_FRONTEND_PORT_HTTP || 8080;
// const PORT_HTTPS = process.env.NODE_FRONTEND_PORT_HTTPS || 443;

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const {AppServerModuleNgFactory, LAZY_MODULE_MAP} = require('./server/main');

const template = readFileSync('./dist/browser/index.html').toString();
app.engine('html', (_, options, callback) => {
    renderModuleFactory(AppServerModuleNgFactory, {
        // Our index.html
        document: template,
        url: options.req.url,
        extraProviders: [
            provideModuleMap(LAZY_MODULE_MAP),
            // make req and response accessible when angular app runs on server
            <ValueProvider>{
                provide: REQUEST,
                useValue: options.req
            },
            <ValueProvider>{
                provide: RESPONSE,
                useValue: options.req.res,
            },
        ]
    }).then(html => {
        callback(null, html);
    });
});

app.set('view engine', 'html');
app.set('views', join(DIST_FOLDER, 'browser'));

function wwwRedirect(req, res, next) {
    if (req.headers.host.slice(0, 4) !== 'www.') {
        return res.redirect(301, 'http://www.' + req.headers.host + req.url);
    }
    next();
}

function indexRedirect(req, res, next) {
    if (req.url.indexOf('/index.html') !== -1) {
        const url: string = 'http://' + req.headers.host + req.url.replace('/index.html', '');
        return res.redirect(301, url);
    }
    next();
}

app.set('trust proxy', true);
app.use(wwwRedirect);
app.use(indexRedirect);

// Server static files from /browser
app.get('*.*', express.static(join(DIST_FOLDER, 'browser'), {
    maxAge: '1y'
}));

// All regular routes use the Universal engine
app.get('*', (req, res) => {
    res.render('index', {req});
});

const httpServer = http.createServer(app);
// const httpsServer = https.createServer(credentials, app);

// Start up the Node server
httpServer.listen(PORT_HTTP, () => {
    console.log(`Node Express server listening on http://localhost:${PORT_HTTP}`);
});

// httpsServer.listen(PORT_HTTPS, () => {
//      console.log(`Node Express server listening on https://localhost:${PORT_HTTPS}`);
// });
